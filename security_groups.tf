resource "aws_security_group" "sg_ssh" {
  vpc_id      = aws_vpc.desafio_vpc.id
  description = "Acesso ssh a instancia"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }
  tags = { Name = "SSH" }
}

resource "aws_security_group" "sg_web" {
  vpc_id      = aws_vpc.desafio_vpc.id
  description = "Acesso pela web"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = "HTTP and HTTPS" }
}

resource "aws_security_group" "sg_hp" {
  description = "Acesso ao dashboard haproxy/stats"
  vpc_id      = aws_vpc.desafio_vpc.id

  ingress {
    description = "Acesso ao stats somente da vpc"
    from_port   = 8088
    to_port     = 8088
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.desafio_vpc.cidr_block]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = "STATS/HAPROXY" }
}

resource "aws_security_group" "sg_vpn" {
  description = "Acesso a VPN"
  vpc_id      = aws_vpc.desafio_vpc.id

  ingress {
    from_port   = 500
    to_port     = 500
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 4500
    to_port     = 4500
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = "VPN_IPSEC" }
}

resource "aws_security_group" "sg_cluster" {
  description = "Acessos requeridos kubernetes Master"
  vpc_id      = aws_vpc.desafio_vpc.id

  ingress {
    description = "Entre as instancias da rede cluster"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_subnet.private.*.cidr_block[0]]
  }
  ingress {
    description = "Porta Ingress"
    from_port = 30003
    to_port   = 30003
    protocol  = "tcp"
    cidr_blocks=["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = "cluster" }
}


# resource "aws_security_group" "sg_ingress" {
#   description = "Acessos requeridos kubernetes worker"
#   vpc_id      = aws_vpc.desafio_vpc.id

#   ingress {
#     description = "Porta Ingress"
#     from_port = 30003
#     to_port   = 30003
#     protocol  = "tcp"
#     cidr_blocks=["0.0.0.0/0"]
#   }
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   tags = { Name = "Ingress" }
# }

resource "aws_security_group" "sg_dash" {
  description = "Acessos dashboard k8s"
  vpc_id      = aws_vpc.desafio_vpc.id

  ingress {
    description = "Porta Dashboard"
    from_port = 30002
    to_port   = 30002
    protocol  = "tcp"
    cidr_blocks=["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = "Dashboard kubernetes" }
}

resource "aws_security_group" "sg_zabbix_ag" {
  vpc_id      = aws_vpc.desafio_vpc.id
  description = "Acesso a monitoramento"

  ingress {
    description = "node zabbix"
    from_port   = 10050
    to_port     = 10050
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = "ZABBIX-AGENTE" }
}

resource "aws_security_group" "sg_zabbix" {
  vpc_id      = aws_vpc.desafio_vpc.id
  description = "Acesso a monitoramento"

  ingress {
    description = "zabbix server"
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = "ZABBIX-SERVER" }
}

resource "aws_security_group" "sg_grafana" {
  vpc_id      = aws_vpc.desafio_vpc.id
  description = "Acesso ao Grafana"

  ingress {
    description = "porta Grafana"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = "GRAFANA" }
}