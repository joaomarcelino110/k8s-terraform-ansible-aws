variable "region" {
  default = "us-west-2"
}

variable "profile" {
  default = "terraform_adm_user"
}

variable "ami_id" {
  default = "ami-010327334690f5fa5" # debian 10.8
}
variable "ami_id_vpn" {
  default = "ami-076cbb27c223df09a" # ubuntu 16.04 LTS
}

variable "ami_type" {
  default = "t3.small" #"t2.nano"   # t2.medium(2vCPUs, 4GB) t3.small(2vCPUS, 2GB)
}
variable "ami_type_light" {
  default = "t2.nano" ## necessario micro
}

variable "subnets_cidr" {
  default = ["10.0.2.0/24", "10.0.3.0/24"]
}

variable "zone" {
  default = "us-west-2a"
}

variable "public_key" {
  default = "~/.ssh/MyKeyPair.pub"
}

variable "private_key" {
  default = "~/.ssh/MyKeyPair.pem"
}

variable "num_nodes" {
  default = 1
}