# VPC para criar as instencias
resource "aws_vpc" "desafio_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    "Name" = "desafio-VPC"
  }
}

/*----------------------------- SET SUBNET PUBLIC -----------------------------*/
resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.desafio_vpc.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true #todas as instancia da subnet terao ip publico
  availability_zone = var.zone

  tags = {
    "Name" = "Subnet public"
  }
}

resource "aws_internet_gateway" "i_gw" {
  vpc_id = aws_vpc.desafio_vpc.id
  tags = {
    Name = "Gateway for Internet"
  }
}
resource "aws_route_table" "rt_public" {
  vpc_id = aws_vpc.desafio_vpc.id

  route {
    cidr_block  = "0.0.0.0/0"
    gateway_id  = aws_internet_gateway.i_gw.id
  }
  
  tags = {
    Name = "Route Table Public"
  }
}
# resource "aws_route" "public_internet_gateway" {
#   route_table_id         = aws_route_table.rt_public.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.i_gw.id
# }

resource "aws_route_table_association" "ass_route_public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.rt_public.id
}

/*----------------------------- SET SUBNET PRIVATE -----------------------------*/
resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.desafio_vpc.id
  count             = length(var.subnets_cidr)
  cidr_block        = element(var.subnets_cidr, count.index)
  availability_zone = var.zone
  tags = {
    Name = "Subnet private - ${count.index + 1}"
  }
}
resource "aws_subnet" "nat_gtw_subnet" { # SUBNET GATEWAY
  vpc_id = aws_vpc.desafio_vpc.id
  cidr_block = "10.0.4.0/24"
  availability_zone = var.zone
  tags = {
    Name = "nat_gtw_subnet"
  }
}
/*----------------------------- SET NAT GATEWAY -----------------------------*/
resource "aws_eip" "nat_eip" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.nat_gtw_subnet.id
  tags = {
    Name = "Gateway for NAT"
  }
}

resource "aws_route_table" "rt_private" {
  vpc_id = aws_vpc.desafio_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }
  tags = {
    Name = "Route Table NAT Gateway"
  }
}

# resource "aws_route" "private_nat_gateway" {
#   route_table_id         = aws_route_table.rt_private.id
#   destination_cidr_block = "0.0.0.0/0"
#   nat_gateway_id         = aws_nat_gateway.nat_gw.id
# }
resource "aws_route_table_association" "nat_gtw_subnet" {
  subnet_id = aws_subnet.nat_gtw_subnet.id
  route_table_id = aws_route_table.rt_public.id
}

resource "aws_route_table_association" "ass_route_private" {
  count          = length(var.subnets_cidr)
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.rt_private.id # se preicsar ligar no gateway de internet precisará MUDAR
}
