resource "aws_resourcegroups_group" "producao" {
  name = "Producao"
  description = "Grupo de recursos para exibir todas as instancias do ambiente de producao"
  resource_query {
    query = <<JSON
{
  "ResourceTypeFilters": [
    "AWS::EC2::Instance"
  ],
  "TagFilters": [
    {
      "Key": "Environment",
      "Values": ["Production"]
    }
  ]
}
JSON
  }
  tags = {
    Name: "Resource Group"
    Environment: "Production"
    Type_Resource_Group: "Instances Only"
  }
} 
