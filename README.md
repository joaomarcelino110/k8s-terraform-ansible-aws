# Provisioning Infrastructure in the AWS with kubernetes, load-balancer (haproxy), monitoring (zabbix and Grafana). All in instances ec2.

# Provisionamento de infraestrutura na AWS com kubernetes, load-balancer (haproxy), monitoramento (zabbix e Grafana). Tudo em instâncias ec2.

Portas utilizadas pelas aplciacoes (para criar os grupos de segurança)

Acesso as instancias ssh    (sg_ssh)
* port  22      tcp     ssh

Acesso pela internet        (sg_web)(sg_ssh)
* port  80      tcp     http
* port  443     tcp     https

Para a instancia HAPROXY    (sg_hp)(sg_ssh)
* port  8088    tcp     haproxy/stats

Para instancia da VPN-IPsec (sg_vpn)(sg_ssh)
* port  500     udp     vpn
* port  4500    udp     vpn
* port  1194    udp     vpn

Para instancia k8s-master   (sg_master)(sg_ssh)
* port  80          tcp     http
* port  8080        tcp     dashboard
* port  6443        tcp     kubernetes API Server
* port  2379-2380   tcp     etcd server
* port  10250       tcp     Kubelet health check
* port  10252       tcp     Kube controller manager
* port  10255       tcp     read only kubelet API

Para instancias k8s-workers (sg_worker)(sg_ssh)
* port  10250       tcp     kubelet health check
* port  30000-32767 tcp     external applications
* port  10255       tcp     kubelet API

Para instancia de monitoramento (sg_mon)(sg_ssh)
* port  80      tcp     http
* port  10051   tcp     Node Port