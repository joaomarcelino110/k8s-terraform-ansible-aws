resource "aws_key_pair" "my_key" {
  key_name    = "MyKeyPair"
  public_key  = file(var.public_key)
}

/* Iniciando as instancias */
resource "aws_instance" "haproxy" {
  ami                    = var.ami_id
  instance_type          = var.ami_type_light
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.sg_ssh.id,
                            aws_security_group.sg_web.id,
                            aws_security_group.sg_hp.id,
                            aws_security_group.sg_zabbix_ag.id]
  subnet_id              = aws_subnet.public.id

  tags = {
    Name = "HAProxy"
    Environment = "Production"
    TypeService = "Balanceador"
  }
}

resource "aws_instance" "vpn_ipsec" {
  ami                    = var.ami_id_vpn
  instance_type          = var.ami_type_light
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.sg_ssh.id,
                            aws_security_group.sg_vpn.id ]
  subnet_id              = aws_subnet.public.id
  
  provisioner "file" {
    source = "vpn.sh"
    destination = "/tmp/vpn.sh"

    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = file(var.private_key)
      host = self.public_ip
    }
  }
  provisioner "remote-exec" {
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = file(var.private_key)
      host = self.public_ip
    }
    inline = [
      "sudo apt-get update",
      "chmod +x /tmp/vpn.sh",
      "sudo /tmp/vpn.sh"
    ]
  }
  
  tags = {
    Name = "VPN_IPsec"
    Environment = "Production"
    TypeService = "VPN"
  }
}

resource "aws_instance" "kubernetes-master" {
  count                  = 1
  ami                    = var.ami_id
  instance_type          = var.ami_type
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.sg_ssh.id,
                            aws_security_group.sg_cluster.id,
                            aws_security_group.sg_dash.id,
                            aws_security_group.sg_zabbix_ag.id]
  subnet_id              = aws_subnet.private.*.id[0]
  tags = {
    Name = "k8s-master"
    Environment = "Production"
    TypeService = "Cluster"
    TypeNode = "Master"
  }
}

resource "aws_instance" "kubernetes-worker" {
  count                  = var.num_nodes
  ami                    = var.ami_id
  instance_type          = var.ami_type
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.sg_ssh.id,
                            aws_security_group.sg_cluster.id,
                            aws_security_group.sg_zabbix_ag.id]
  subnet_id              = aws_subnet.private.*.id[0]
  
  tags = {
    Name = "k8s-worker-${count.index}"
    Environment = "Production"
    TypeService = "Cluster"
    TypeNode = "Worker"
  }
}

resource "aws_instance" "zabbix" {
  count                  = 1
  ami                    = var.ami_id
  instance_type          = var.ami_type 
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.sg_ssh.id,
                            aws_security_group.sg_zabbix_ag.id,
                            aws_security_group.sg_zabbix.id,
                            aws_security_group.sg_grafana.id]
  subnet_id              = aws_subnet.private.*.id[1]
  
  tags = {
    Name = "zabbix"
    Environment = "Production"
    TypeService = "Monitoramento"
  }
}

resource "aws_instance" "grafana" {
  count                  = 1
  ami                    = var.ami_id
  instance_type          = var.ami_type_light
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.sg_ssh.id,
                            aws_security_group.sg_zabbix_ag.id,
                            aws_security_group.sg_grafana.id]
  subnet_id              = aws_subnet.private.*.id[1]
  
  tags = {
    Name = "grafana"
    Environment = "Production"
    TypeService = "Monitoramento"
  }
}

  
  # provisioner "remote-exec" {
  #   inline = ["sudo hostnamectl set-hostname --static ${aws_instance.haproxy.tags.Name}"]
  # }