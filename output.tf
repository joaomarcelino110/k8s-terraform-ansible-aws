/* ------------- IP INSTANCIAS ------------- */
output "publicIP_HAProxy" {
  description = "IP publico das instancias da rede pblica"
  value       = aws_instance.haproxy.public_ip
}

output "privateIP_HAProxy" {
  description = "IP privado do haproxy"
  value       = aws_instance.haproxy.private_ip  
}

output "publicIP_VPN_IPsec" {
  description = "IP publico das instancias da rede pblica"
  value       = aws_instance.vpn_ipsec.public_ip
}

output "privateIP_vpnIPsec" {
  description = "Ip privado do apache"
  value       = aws_instance.vpn_ipsec.private_ip
}

output "privateIP_k8s-master" {
  description = "IP privado das instancias da rede privada"
  value       = concat(aws_instance.kubernetes-master.*.private_ip)
}
output "privateIP_k8s-workers" {
  description = "IP privado das instancias da rede privada"
  value       = concat(aws_instance.kubernetes-worker.*.private_ip)
}
output "privateIP_grafana" {
  description = "IP privado das instancias da rede privada"
  value       = concat(aws_instance.grafana.*.private_ip)
}
output "privateIP_zabbix" {
  description = "IP privado das instancias da rede privada"
  value       = concat(aws_instance.zabbix.*.private_ip)
}

resource "local_file" "AnsibleInvetory" {
  content = templatefile("inventory.tmpl",{
    haproxy-priv = aws_instance.haproxy.private_ip,
    kube-master = aws_instance.kubernetes-master.*.private_ip[0],
    kube-worker = aws_instance.kubernetes-worker.*.private_ip,
    zabbix = aws_instance.zabbix.*.private_ip[0]
    grafana = aws_instance.grafana.*.private_ip[0]
  })
  filename = "inventory.yml"
}

resource "local_file" "HaproxyEntry" {
  content = templatefile("./roles/balancer/templates/template-haproxy.cfg",{
    kube-master   = aws_instance.kubernetes-master.*.private_ip[0],
    kube-worker   = aws_instance.kubernetes-worker.*.private_ip,
    zabbix        = aws_instance.zabbix.*.private_ip[0],
    grafana       = aws_instance.grafana.*.private_ip[0]
  })
  filename = "./roles/balancer/templates/haproxy.cfg"
}

resource "local_file" "AgentZabbix" {
  content = templatefile("./roles/common/agent-zabbix/templates/agent-template",{
    zabbix = aws_instance.zabbix.*.private_ip[0]
  })
  filename = "./roles/common/agent-zabbix/templates/zabbix_agentd.conf"
}

output "subnet" {
  value = aws_subnet.private.0.cidr_block
}